# solarus-steam-resources
For Solaruse games using Steam achievements and other Steamworks API features, these files need to exist alongside `solarus-run`, or the launcher or editor; whichever is used to start your game.

`steam_appid.txt` needs to have your game's Steam App ID, which can be found in the URL of your Steam page.

Luasteam is an open-source, MIT licensed library to use lua code to interact with the Steamworks API.
For information on Luasteam, see https://github.com/uspgamedev/luasteam

steam_api64.dll is the distributible version of the steamworks SDK for windows 64bit. See https://partner.steamgames.com/doc/sdk